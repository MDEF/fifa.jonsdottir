---
title: Computer aided design
period: 30 January - 6 February 2019
date: 2019-02-06 12:00:00
term: 2
published: true
---

I started this week with the goal of modeling something in 3D, my previous attempts at 3d modeling  using Rhino were complete failures so I was not really looking forward to this week. During the tutorials class we got an introduction to Fusion 360° which is a program extension from Autodesk. My first experience with Fusion was so much better than with Rhino. For one I can move around in the program with relative ease. I understand that Rhino is a powerful 3d modeling tool but Fusion is really beginner friendly which I appreciate.

During the tutorial I was able to model the lego brick almost completely on the first try (following along the tutorial) Being able to create something that looked like the object in front of me was a relief so I’m feeling a bit better with the 3D modeling part of fab academy.

At home I wanted to get more familiar with the program so I found a very basic tutorial on parametric design using Fusion

[YouTube tutorial](https://www.youtube.com/watch?v=Uel_NmlwdoA&t=302s)

It was an in depth tutorial on how to create the most basic shapes by creating sketches and connect them with user created parameters. Even though it was in depth the tutor skipped explaining some things like how to edit sketches. After I created the box I wanted to make it taller but I could not find where to do it so it took me a while to figure out how to select the right surface to edit. I tried going through all the commands but what finally worked for me was when I selected to the right not sketches but bodies. When I selected “body 1” I could select only the top surface and edit the height of the box.

![]({{site.baseurl}}/trouble extruding.png)

When I had created the box with the circle cut out I added the parameters (changing parameters is in the modify tab all the way down at the bottom) - indicating the width, height and depth also wall thickness and the circle radius.

![]({{site.baseurl}}/user parameters.png)

Having created the parameters I can now edit all the sizes of the box by entering the words in where the numeric values were. In the field where you edit the length you can enter the user defined parameters but also enter equations, which was helpful when I centered the cutout.

![]({{site.baseurl}}/cutout equation.png)

Modeling in Fusion 360 is a lot nicer than in Rhino and I think it is perfect for a beginner. It is very intuitive and I can imagine learning more and using it for other projects.

[Fusion design file]({{site.baseurl}}/box.step.zip)
