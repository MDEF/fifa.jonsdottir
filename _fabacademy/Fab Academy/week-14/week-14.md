---
title: Wild card week - Welding
period: 20 - 27 May 2019
date: 2019-05-27 12:00:00
term: 2
published: true
---
## How does a welding machine work?

The welding machine we used works using electricity. It is basically a series of controlled explosions that melt the filament. The machine is polarized: so the filament that comes out of the welding gun has a positive charge and then the negative charge is provided by a ground cable which is attached to the metal working surface. We attached the ground cable to the clamp that secured the two metal pieces to the table.

When the filament is extruded, gas is also expelled from the welding gun. the gas does not act as a source of fire but it keeps  the environment surrounding the filament more stable.

The filament is connected all the way through the welding gun to the machine so you need to move the welding gun in slow smooth motions so you don’t break the filament within the tube. It is hard to rewire the filament.

## Safety

- When working with welding you need to take precaution because welding creates some extreme environments.

- The first important thing is to protect your hands and make sure that you never touch the ground and the filament or the tip of the welding gun at the same time when the machine is on because of the high voltage.

- Always wear leather gloves when welding because rubber gloves can melt. The leather gloves only partly provide protection from the heat that is generated so always be aware of hot objects and surfaces.

- You always need to wear fire retardant jackets when welding because the sparks that are created can ignite your clothing if not careful.

- The second most important thing is to protect your eyes. Looking at the welding sparks without the specially made helmets can permanently damage your eyesight so when you are welding you always need to make sure that your eyes are protected and if you have an audience to ask if everyone is ready before starting the welding.

- There are two types of helmets, one is digital and senses the brightness of the sparks and automatically turns the screen dark. The other type of helmet does not have a sensor so it needs to be put down to protect the eyesight.

![]({{site.baseurl}}/equipment.jpg)

## Welding

![]({{site.baseurl}}/weld.gif)

- When starting the welding you need to be sure that the pieces that you will be welding together are secured tightly to the working surface.

- Check the length of the filament. It should be around 1 cm in length from the tip of the welding gun and not have a melted ball on the end. If there is a ball of solder on the end you need to trim it off.

- If the filament is too long or if there is a ball on the end it might cause the solder to jump and splatter over the surface which causes a bad weld and can also be dangerous to others around.

- Press the trigger and it will start to spark. You should wait around a second until the process settles. You can hear the change. It should stop sparking and turn into more of a purr.

- You will see a red ball of molten metal. it should follow you while you move the gun down. if it does not then you are moving too fast.

- Move the gun in small slow circles so you create the most even weld. If you are successful then you should have an even line that looks a bit like a mountain.

![]({{site.baseurl}}/weldingact.jpg)

## Finishing

When you are done welding then you can move on to grinding the seam to create a more even surface.

The grinder that we used was defective so when Mikel bent down to plug it in it started rotating even though the button was in the off position and whirred across the table, onto the floor. Thankfully nobody was in the way of the machine but it reminds you of always being careful around these kinds of machines because there can always something go wrong.

Grind at a 45 degree angle, making the sparks come towards you.

![]({{site.baseurl}}/sparks.jpg)

We set up the other grinder and finished off our welds. When you grind it down to the level of the two parts you can see if your weld was good. I saw that mine had a hole in the middle but otherwise it was fine.

One more thing was that I ground the weld down too far. If you grind into the two pieces they will be thinner than the rest and will not be as strong so you should make sure that you are not grinding too far.

![]({{site.baseurl}}/weldingseam.jpg)
