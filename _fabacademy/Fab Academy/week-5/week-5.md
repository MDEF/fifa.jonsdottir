---
title: 3D printing and scanning
period: 20 - 27 February 2019
date: 2019-02-27 12:00:00
term: 2
published: true
---

## Printing

I am really interested in the possible applications of 3D printing, especially the flexible material and the resin printer. I want to model something myself but I think it’s going to take a while to do it so I downloaded a small geometric plant pot off of thingiverse to have something printed for the documentation. I also wanted it to be something that I could use so I wasn’t just wasting material. The pot came out really nice and I am happy to have it in my apartment. It is going to be a nice home for a cute little plant.

![]({{site.baseurl}}/3D drawing.png)
[Original files by Sharon2310](https://www.thingiverse.com/thing:2508690)

When setting up the print the rotation of the object matters for the final result so if you want the outside to have a nicer finish then the object should face down, so the side that is facing up will have a smoother finish. If you wanted to make a mould then you would have the object facing up with supports on the lower side.

This object can not be made with substractive methods because the edges taper inwards so the dimensions inside the object are wider than the opening.

I wanted the object to have a smoother finish on the outside so I rotated the object to face down. the lowest angle was around 45° so it did not need any supports.

I used:
- Cura - Ultimaker
- PLA (white, glow in the dark filament)
- Line height: 0,2 mm
- Extruder size: 0,4 mm
- Infill: for my object it was enough to have the infill 20% because the walls    were quite thin and the object was quite sturdy
- Enable fans for cooling
- Temperature for the PLA was 205 degrees for the filament and 40 degrees for the build plate
- Build plate adhesion: brim

Setting up the build you remove the glass plate and spray it with a spray called nelly, you spray it with just enough spray so it’s coated but not so much that it looks wet. Spraying the surface makes it easier to extract the print.

![]({{site.baseurl}}/ultimaker.jpg)

Printing the object took 4 and a half hours so I let it run overnight. When I came back in the morning it was ready and looked perfect.

![]({{site.baseurl}}/finished shape.jpg)

when removing the print take the glass plate out and use a spade to pry it up on a table, don’t try to remove it on the build plate because if you put pressure on the plate you can mess up the accuracy of the plate.

![]({{site.baseurl}}/3D result.jpg)

I really want to try the resin printer with something I have modelled myself but it's taking a long time to figure out how to create the specific shape I want so I'm just going to take my time and figure out how to do it on my own because I really want to learn how to do it by myself even if it takes longer.

## Designing my own object

I made a simple design that will be useful for me in the process of molding and casting so I created this corner piece that will be easy to cast.

I created the shapes in fusion 10x10x10 cm using the Chamfer function to create shaved edges. The edges are shaved at a 45° angle 3 mm in. I started by creating the basic blocks and then I put a hole in the inner corner to mimic the styrofoam corner that I modelled this shape after but when I thought about it, it would probably be problematic when casting it so I took it out. I finished the shape by shaving all the edges except the ones at the base because they will make it harder to cast as well.

![]({{site.baseurl}}/holeincorner.png)
hole design that did not work

![]({{site.baseurl}}/chamfer.png)
chamfer function

![]({{site.baseurl}}/boxfini.png)
finished design - [Box STL file]({{site.baseurl}}/styro corner.stl)

To export the file go to the tool bar and press the make button. select 3D print and in the pop up window deselect the box that says send to 3D print utility. When you have done that press OK and it will save as an STL file.

![]({{site.baseurl}}/sendtoprint.png)

## Printing my own design

I used the Ultimaker 2 but when I went to turn it on I noticed that the filament wasn’t connected but there was still material in the tube going down to the nozzle. I had to ask how to remove the material and I had to disconnect the tube at both ends (by pressing down the white circle) and remove it from the machine because the filament was broken at both ends. I used a steel wire to push the broken filament out of the tube. When I tried to insert the new filament into the machine it didn’t work. The filament had broken inside the feeding mechanism. I had to get Mikel and Oscar to remove all the broken pieces from the feeding mechanism and the nozzle. When the new filament was ready the printer was ready to print.

![]({{site.baseurl}}/filament.jpg)

I adjusted the line width to 0,4 mm and the infill to cubic and 10%. The part does not need to be very dense and it won’t hold any structure so it doesn’t need much support on the inside. Press prepare and save to external drive.

![]({{site.baseurl}}/boxprint.JPG)

![]({{site.baseurl}}/styrocornerfin.jpg)
Finished printed object.

## Scanning

For the 3D scanning I used the kinect camera to scan and the program I used was scanect that is connected with the camera. I tried scanning my 3D printed object first but it did not show up on the camera. It probably was too shiny and too small so I decided to scan myself with the help of Barbara. I sat on a desk chair and mounted the camera on top of a paper roll so it was in the correct position.

after setting up the scanect program, we tried to connect the camera but it did not work at first so we had to change the camera to the one that is labeled santifu. After changing the cameras it connected and I was ready to scan.

 ![]({{site.baseurl}}/scanning setup.jpg)

When scanning, you have to rotate either the object or the camera and I rotated myself on the chair in front of the camera and then Barbara helped me to scan the top of my head. I think I turned a little too fast so my nose did not register fully so it looks a bit like I don't have a nose... so make sure to rotate slowly.

![]({{site.baseurl}}/me scanned.jpg)
_it looks a bit terrifying_
