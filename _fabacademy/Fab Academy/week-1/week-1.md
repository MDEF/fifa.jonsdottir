---
title: Project management
period: 23 - 30 January 2019
date: 2019-01-23 12:00:00
term: 1
published: true
---

The first week of Fab academy for me was an experience. I had been very nervous about it because I don’t have much experience in digital fabrication and electronics. Also because I didn’t have an idea for a final project for the fab academy so I was hesitant to start thinking about it. I think after going through some of the previous fab academy projects I feel a little bit better because I can see that I don’t have to connect every single week to the final project.

The things I’m most nervous about working on 3D programs because my previous experience with Rhino was awful. I am much more comfortable working with two dimensional programs, so i might try to create the product in two dimensions and then import it into a 3D editor.

It felt like it was starting slow but maybe that was because we already have a git lab website up and running. It was also a relief that we won’t be called to explain our project to the entire Fab network. It makes me feel more excited about it.

There were some interruptions during the video conference but when you think about that there are tens to a hundred computers connected to the conference from all over the world. It is kind of amazing compared to that using skype or other video call applications sometimes do not work that well between two computers. So that was very impressive to me.


### Creating an extra tab for Fab Academy on our jekyll layout website.

From the first term we already had an existing git lab website running. I use hyper as my terminal and atom as the Git editor.

It is really helpful to be able to work locally so you don’t have to commit to the changes you are trying out so you can work faster without having to push every time. To work locally you have to write “cd” in your terminal and then drag the folder you want to work in (fifa.jonsdottir for me) then you write “bundle exec jekyll serve” and press enter. When you have done that, you get a web link that you can enter in your browser and then you are able see your changes in atom directly before you push.

Because it was a pre-made template, navigating through it takes a while and understanding how everything is linked takes quite some time. When I was trying to create a new tab for the fab academy I had some help from Gabriela, Veronica and Barbara. They explained to me what files I needed to copy and how to link it.

The first step was to create a new markdown file for the fab academy and copy the three sections on the top.

![]({{site.baseurl}}/fabmarkdown.png)


Step two is to duplicate the reflections HTML file and rename it fabacademy. Then replace all the reflections with the word fab academy. Use command F to find and replace all the words. That was my mistake. I missed one of the words and when it didn’t work it was because I had missed that one word. After fixing that problem with the html file the tab worked.


![]({{site.baseurl}}/fabhtml.png)

Step three is to create a "fabacademy" folder and create a folder structure within that folder.

![]({{site.baseurl}}/tabcreation.png)

Step four: When the changes to the markdown file, HTML file, and the config.yml file are done and you have created a separate folder for the tab you have to go to the header.html file and add a link to the navigation bar.
That should be the last step of the process!

![]({{site.baseurl}}/fab header.png)

## Updating your page

In Atom (my chosen editor) you can update your page. on the left you can see the structure of your pages, in the middle you have the editing section and on the right you have the opportunity to have the git tab. To have the git tab appear you go into view in the navigation bar and press toggle git tab then a section appears that allows you to submit your changes. You can do this in steps. first you save your md file by pressing command & S on Mac. The changes will appear in the unstaged section. Then you press

- Stage all
- Write a commit message, to remind you of what the change was.
- Press Commit to master
- Press push at the bottom of the screen.

When you have finished those steps you can check on your Gitlab page in pipelines the status of your commit.

## Final project sketch

I am creating a kit with a mask and testing equipment

![]({{site.baseurl}}/finprojectsketch.jpg)
