---
title: Applications and implications
period: 17 - 23 April 2019
date: 2019-04-29 12:00:00
term: 2
published: true
---
![]({{site.baseurl}}/facegrid.png)

### Define the scope of your project
I am going to create a kit for sampling microbiomes. The kit will include four different sections for the different components. A mask, Swabs with corresponding tubes, pH strips for testing and a thermal camera stand-in.

The subject of my project is the microbiome and it is focused on biology and human perception of that biology. I want to make a connection between humans and their microbiome and I intend to do that through a sampling kit that I will create with digital manufacturing processes.

The most important part for my kit is to create the mask because it needs to be something that is easy to use and not feel to foreign. The second part that needs a lot of attention is the design of the swabs and how they are stored because even though the kit will not serve a testing function in the exhibition it needs to seem sterile and feel logical to the user.

The kit will be an introduction to my narrative and takes something that already exists and improves the design and the interaction. The kit is designed to make the testing of different sections of your skin more organized and consistent.

### What has been done before?

There exist a number of kits out there already, The kits usually include sterile cotton swabs and tube containers. They also include

One kit is from the Human microbiome project and that kit came in an envelope with 6 pages of instructions. One tube with two sterile swabs and a checklist of what needs to be completed before you send it back.

The other kit is from a private company called UBiome. they have a few kits available based on what you want to test but the foundation is the same. Sterile cotton swabs and tubes that serve as containers. On pictures it is a box with cotton swabs and tubes that contain liquid. There are also indents where the tubes can be placed so it is easier to work with them.

I found one kit in Shenzhen that was a sampling kit for the skin. I could not read most of the text but it seemed like the purpose of the kit was to sample the skin and then send it in so the company responsible could create perfume based on the composition of the skin.

The kits are quite simplistic and differently designed. I want my kit to be simple but also collect more information than the kits that already exist.

### What materials will you need (Bill of materials)

- Box: heavy cardboard
- Structure inside the box: Either cardboard or acrylic
- Mask: To be determined
- Testing material: Swabs and matching tubes.
- Stand in for thermal camera (might use a 3D printed addition to replicate)
- pH strips

### What parts and systems will be made? What processes used?

Mostly I will use the laser cutter to create the box and the structure inside the box. I might use the 3D printer to create the placeholders for the tubes. I might also use the vinyl cutter to create a  professional looking finish on the outside of the box.

### What tasks need to be completed.

I need to finish the prototyping of the grid mask to figure out how big and thick the box has to be. I want the box to be quite thin and simple but it all hinges on how the mask will look because that is what takes up the most space.

The main questions that need to be answered are how will the mask look and how can I organise the box so it is as easy to understand and use as possible. I also need to figure out a way to keep everything sterile. How would the best way of handling the tubes and how to pack everything up after it is used.

### What is the schedule? real vs ideal

The most important thing is to finish the prototyping before moving on to creating the files for the digital fabrication. I do not have much time for the prototyping so I am trying to do some really basic and fast prototyping. The ones I have made already are not ideal because they are big and hard to pack so I will try to make something that takes less space. Ideally I would have the kit before the 27th of May but I have to see how the work goes.

### How will it be evaluated?

The kit will be evaluated by tutors and then left at the exhibition for visitors to interact with. I want the kit to feel approachable and sturdy enough so it survives two weeks at the exhibition.
