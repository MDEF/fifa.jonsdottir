---
title: Electronics production
period: 13 - 20 February 2019
date: 2019-02-20 12:00:00
term: 2
published: true
---

Assignment:

- Mill a PCB board and solder it.
- Program the board so it shows up as USB on your computer

important is do not call the end mill a drill bit
end mill: removes material
drill bits: makes holes

## Milling:

Make sure that the machine surface is clean, brush off excess dust and wipe it off with alcohol. Place double sided tape on the bottom of the PCB copper sheet, without the tape overlapping each other. Extend the tape a bit over the edge so you can cut away the excess and have a flat straight tape on the bottom. You place the sheet glue side down as parallel to the machine as possible and press it down firmly so it won’t move when milling. when the sheet is ready you can put the end mill in place.

When you are handling the end mill you have to be careful because it is really fragile. Take the end mill out of it’s box keeping the blue plastic cap on until you have fastened it with an allen key. When you are ready you can remove the cap and adjust the z position of the mill. You should move it carefully, supporting the mill with your finger. When close to the surface, unfasten the mill with the allen key and lower the mill with your finger until it touches the copper.

![]({{site.baseurl}}/millinginterface.jpg)

When everything is ready you can set the x and y coordinates, not too close to the edges. use bit 1/64 to mill the board and bit 1/32 to mill the outlines. if you can see dust forming that is good because it should mill into the plastic under the copper. Mill one at a time because the sheet could move. When changing the bits you have to reset the z coordinates in the lower left corner and then the milling will start from the upper left corner.

![]({{site.baseurl}}/milling.jpg)

Note: make sure that the machine is in a position not too low so it can move down when milling the outline.

Note: creating a file with enough offsets but not too much because then it will become too hard to solder.


## Soldering

Tools and equipment for soldering:
- silicon mat
- soldering iron
- wet sponge for cleaning the iron
- tweezers
- solder
- wick to remove solder
- PCB and components
- Flux (makes delicate soldering easier)

Before starting you should write down all the components on a piece of paper and collect them, glueing them with clear tape to the corresponding places so you use the correct pieces. after taking the component out of the drawers you should update the inventory sheet!

![]({{site.baseurl}}/components.jpg)

Soldering: Turn on the iron. Place the soldering iron onto the copper surface before introducing the solder, the surface should be hot so the solder will adhere. You need to “tin” the surface before placing the component on the board by putting a small amount of solder on the board

Work from the center of the board because it can be hard to solder in narrow spaces. The solder should cover the copper and look shiny and nice. If you mess up you can remove solder with the wick by placing the wick on top of the solder and placing the soldering iron on top of the wick and waiting until the liquid solder is absorbed. to see if you soldered correctly you can wash the board with paper and alcohol and then use the multimeter to check if you have shorts anywhere.

I have not done that yet. - The USB port is also missing from the picture because the component was not in stock when we were soldering it.

![]({{site.baseurl}}/solderfabisp.jpg)

## Final result

![]({{site.baseurl}}/electronicsoverview.png)
_this is without the micro USB, I added that later when the part was available_

## Programming the PCB

To program the FabISP I downloaded the firmware for MAC and followed the steps of the tutorial. Open Hyper or other text editor typing “make clean”, “make hex”, “make fuse” and lastly “make program”. My programming didn't work in the beginning but it was because I was not supposed to follow the step where it said add # before the avrisp2 because we were using the MKII to program the board so that step was unnecessary. After finding that mistake my board worked.

![]({{site.baseurl}}/mistake.png)

![]({{site.baseurl}}/programfabisp.jpg)

Now my ISP shows up in my computer as a USB

![]({{site.baseurl}}/programmed board.png)

[FabISP assembly instructions](http://archive.fabacademy.org/archives/2017/doc/electronics_production_FabISP.html)

[Programming tutorial](http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html)
