---
title: Computer Controlled Cutting
period: 6 - 13 February 2019
date: 2019-02-13 12:00:00
term: 2
published: true
---

Using the vinyl cutter was my favourite this week! the machine is really easy to operate and the result from the vinyl looks so good.
The most surprising part for me was that you are able to do screen printing using the vinyl cutter. I had no idea you could do that.

Me and Barbara wanted to make a birthday present for Ryota. Barbara had talked about making stickers but when we heard that we could use the vinyl cutter to do screen printing we knew we wanted to do something with that. We made a sticker using the threshold image editing tool in photoshop. First I tried using the image trace in illustrator but the image became too complex to use as a sticker so I used that for the engraving project instead.

I took the black and white photo from photoshop and imported it into illustrator where I image traced the result to make a vector. I added the text and expanded it so it turned into vectors too. When the image was ready I exported it as a .dxf file for compatibility.

Preparing the vinyl cutter for use was very simple. just turn it on, measure your material and place it underneath the bar, making sure that both of the wheels lie on the material. In the software you need to write in the width and height of your canvas and then you can import the dxf file into the program, resize it and move it so it sits in the right place on your material. When everything is ready you can press cut and that’s it. Super easy and fun!

![]({{site.baseurl}}/vinylcutter.jpg)

We were preparing the file to use for screen printing but we forgot that we needed to flip the image so we flipped it in the program just by right clicking and choosing the flip horizontally option and cut again.

When the image was cut I removed all the unwanted bits and then I placed masking tape over everything so it would be easier to transfer on to the screen. when the image had been transferred and the masking tape removed I placed clear tape to block out the rest of the screen. When the screen was ready for print the screen is paced face down on the material and then paste (paint) is put in a line on the back of the screen and then pressed with the squeegee over the screen and then back as hard as possible.

![]({{site.baseurl}}/screenprint1.jpg)
<br>
![]({{site.baseurl}}/screenprint2.jpg)

Things that went wrong. we did not have anyone pinning the frame down so when I dragged the tool over the screen it moved. I also made the mistake that I put the paint over the frame again after lifting it up and that ruined it. we had to wash it after that and then the stickers came loose so make sure not to do that.

We had to redo all of it but after doing it the first time the second time went a lot faster and we were able to finish it in the afternoon. The final result was really nice and I am still kind of amazed that you can do it like that, without the emulsion and development and that makes the whole process so much faster. You can only use the screens with the vinyl stickers once or twice but if you are making small projects it is a lot easier than the normal method.
<br>
![]({{site.baseurl}}/screenprint3.jpg)

![]({{site.baseurl}}/screenprint4.jpg)

![]({{site.baseurl}}/screenprint5.jpg)

[Illustrator drawing]({{site.baseurl}}/ryotabirthday.ai.zip)

[DXF file]({{site.baseurl}}/ryotabirthday.dxf.zip)


## Fab academy laser cutting

The group project for this week was to test out the laser cutter and how the machine works with material.

![]({{site.baseurl}}/Press fit.jpg)

The press fit test was to see different ways of joining the material, we did not have to add a margin because of the material that the machine cuts away (which is called the kerf)

![]({{site.baseurl}}/kerftest.jpg)

The kerf test was to see how much material the laser burns away. This is used when you want really flush joints and can calculate the kerf into your design.

we did an engraving assignment, and a  kerf test. The engraving was a little interesting. When I exported the illustrator file to DXF it didn't read the black color so it was missing from the engraving. It came out pretty well though.

![]({{site.baseurl}}/rastertest.jpg)

[Engraving SVG file]({{site.baseurl}}/ryota.svg.zip)

Press fit kit: T-Rex (from CNC week)

We made the file ready for the CNC but then it is better to test it out in the laser cutter before cutting it on a large scale. So we took the files and put them into Rhino and scaled them down to 1/5th of the size which fit perfectly for 3mm plywood.

We had prepared the file with parametric tabs where the original width of the tabs were 15 mm. When we scaled the file down we could easily change the width of the tabs to exactly 3 mm. The parameters are set in the modify tab. It was a long process because you set the parameters individually but it's worth it so you can change the width of all the tabs as you need. Whether you need to scale the drawing down or add a tolerance.

We didn't test the material first so when we did the first try we had the speed at 1.5 but it was too fast to cut all the way through so we had to cut it all over again but the second time it came out perfectly.

The issue we had the first time with the laser was that the board was bent just a little bit so the cut was dark and smoking in some places and in others it was better focused. The focus of the laser was a little bit off on the whole sheet so we reset the focus and secured the second sheet with more tape and slowed down the laser. After those adjustments we got a clean cut on the whole dinosaur.

- Power: 65
- Speed: 1.0
- Frequency: 1000

![]({{site.baseurl}}/pressfit1.jpg)

![]({{site.baseurl}}/pressfit2.jpg)

## Final result:

![]({{site.baseurl}}/laserdino.png)

[Dino DXF file]({{site.baseurl}}/dino.dxf.zip)
