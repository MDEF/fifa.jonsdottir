---
title: Input devices
period: 27 March - 3 April 2019
date: 2019-04-03 12:00:00
term: 2
published: true
---

Bio indicators. - how do we make sense of the world around us.

Go for sensors that have the best documentation.
[Sensor resource](https://learn.adafruit.com)

## Environmental impact

[Environmental impact of lithium mining](https://www.wired.co.uk/article/lithium-batteries-environment-impact)

After experiencing a carefree environment regarding cheap, readily available sensors and technologies I want to read more about how our consumption of electronic parts and devices impact our environment.

Biological clean up solutions.

[Algae used to absorb mining pollution ](https://phys.org/news/2019-03-peru-pollution-green-algae.html)

I know that technology can be amazing and helpful but I think that it should be used sparingly so we do not overwhelm the planet. Our appetites for technology and the ubiquity of the internet of things is something that we should be wary of.
