---
title: Electronics design
period: 27 February - 6 March 2019
date: 2019-03-06 12:00:00
term: 2
published: true
---

Designing our own PCB - You have the choice between two programs when designing the boards, Eagle and Kicad. I chose Eagle because I like the graphic interface. It almost feels like a brainteaser trying to organise all the components so you can connect them.

![]({{site.baseurl}}/eagle diagram.png)

![]({{site.baseurl}}/pcbconnections.png)

Move from the sketch to the schematic (it will look like a mess at first)

![]({{site.baseurl}}/ratsnest.png)

use a tool called ratsnest to try to untangle the components a little bit

![]({{site.baseurl}}/noerrors.png)

when you have connected all the components you go into the tool bar and click the DRC option. There you can see if you have any errors.

![]({{site.baseurl}}/pcb layout.png)

Make sure that there is enough space between the lines because otherwise you could have trouble soldering the components or the mill could ignore the space and not cut the lines.

## Adding components

We had to add some components in Eagle so the board got a lot more complicated. The parts that were missing were the FDTI connector, LEDs and resistors for those LEDs. We also added a 20 MHz resonator. All those extra components made organising the board a lot harder.

![]({{site.baseurl}}/layoutecco.png)

![]({{site.baseurl}}/troubleshooting.png)

Checking the DRC function you can see where your board has errors and clean it up accordingly. Always check it two or three times because every adjustment you make can mess up another part of your schematic.

![]({{site.baseurl}}/layoutadjusted.png)

I had to adjust my layout because two of the wires were too close together for the machine to mill. So I added one jumper and directed the VCC path to the outside of the board.

## Manufacturing

![]({{site.baseurl}}/vinylecco-min.jpg)

I tried vinyl cutting the board with the roland vinyl cutter on a copper sheet glued to sturdy see through vinyl. There was a bubble on the sheet so one of the paths got messed up so I couldn't solder it. Some of the paths were also super thin so I don't think it would have worked anyway.

I ended up milling the board. I had some problems with it but I ended up with a good board in the end.

- The exported PNG files from Eagle are double the size on MAC so you have to size them down in Photoshop.
- The sacrificial layer in the milling machine was uneven so I had to manually lower the Z axis while it was milling. It cut a bit deep so I had slow the speed down to 60 so I wouldn't break the endmill.

![]({{site.baseurl}}/outlinemistake.png)

I went into fab modules and calculated the traces with 4 offsets.
My outline file did not come out right from photoshop so it was missing one of the sides. There should have been black lines all around but there was a red line instead. I didn’t understand that it was not a whole circle. I found out when it was cutting that it was not cutting one side.

I just cut the last side with the saw.

![]({{site.baseurl}}/milledecco-min.jpg)

## Soldering

After ending up milling the board the soldering was a lot easier than trying to solder the vinyl cut one.

Soldering the board was easier than the FabISP board. There was more space for the components and having done it once before it felt a lot simpler.

There were two components that were a little bit tricky. The FDTI part had to be soldered on an angle because the jumper was underneath but it worked
even though it was a little bit elevated. The second tricky part was the resonator. It is a little cube with three places to solder but if you put solder on the copper before hand and then connect it on the edges it will be easier.

List of components for The HelloEcco board.
- Resistor 499  x2
- Resistor 10K  x2
- Resistor 0    x3
- Led Red       x2
- Capacitor 1uF
- Resonator 20 MHz
- Micro controller ATTiny 44
- Button
- AvRISP MD 2x3 pin
- FDTI 6 pin

![]({{site.baseurl}}/solderedecco-min.jpg)

[Eagle board file]({{site.baseurl}}/eagleboard.zip)
