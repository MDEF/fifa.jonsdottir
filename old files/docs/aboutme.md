### About me

![](assets1/aboutmepicture.jpeg)

My name is Fífa Jónsdóttir and I'm a Communication Designer from Reykjavík, Iceland. I am very interested in the human side of design and how to implement design into society.
Designer by trade, biologist at heart.
