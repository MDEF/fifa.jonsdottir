# Introduction to Futures





*What is the future? Tomorrow? 5 years? 10.000?*



The future is quite an abstract concept and changes between cultures. The current version (or the one we are familiar with) has been formed in western society through literature and media for the last 150 years. It is strange to think of a world where the concept of future does not exist but that is the reality of some cultures. Do we need the idea of future to progress?



Designers are future builders in a sense that most strive to make a better world with their designs and work by anticipating need and want.  It is easy to stay theoretical and speculate how the designs will solve every problem but in reality it is not that simple. When thinking of the future it often goes to the extremes either to Utopias or Dystopias. But as we discovered this week there are almost endless versions of the futures that span the whole scale.

![](assets/futures from 1900.jpeg)

We did an interesting experiment where we created speculative scenarios with some prompts. Ours were Post-human and Post-identity with a book as an important object. We talked about how a future with gender as a completely free expression and if that were somehow connected to either a biological or technological change in humans.

We found that gender is quite related to language so at first we talked about a biological recipe book that you could follow to edit your gender but then we started talking about gender and religion and if religious leaders would come together and make the language of the bible and other religious literature gender neutral. That event would spark a whole movement of free gender expression and with it technological augmentations to ourselves. That then would shift the religious aspect over completely to tech companies where the programmers would be priests talking to AI (the holy ghost) and the stores would become a place of worship. When we talked about that we realized that it is almost our reality which was kind of scary.

![](assets/posthuman.png)

[Gender is not as concrete as we thought - Podcast](https://www.wnycstudios.org/story/gonads-xy)



### Synthetic biology – The way to a post human future?



*Can we edit ourselves?*

A post-human future sounds strange and even a bit scary but it is becoming a reality right now. In theory we have endless capabilities to edit humans. The technology is there but research and knowledge of implications is not, so we are at the gateway of a post human future. It does not have to be a bad thing but we need to tread carefully for our own sake.



Synthetic biology relies heavily on future speculation as a tool to inform the public and for their technology to be embraced by society. It may not always be realistic and those speculations are definitely not always accurate but they provoke thought and debate which is crucial to prepare people for the future.



[Speculating a biological future](https://motherboard.vice.com/en_us/article/yp3emj/synthetic-biology-is-speculative-fiction)



### Human - bacteria symbiosis as Trans humanism?

My research these last few weeks have lead me to look into the connection between humans and their microbiome – focusing on the flora of microbes that live on our skin. And how that differs from the relationship we have with our internal flora. It is not new technology, but awareness of the things that live with you and knowledge of how to take care of that relationship could be considered trans-human.



I talked to a lot of people and explained my concept to them and I got very positive responses and I think that was because I was able to reference actions and habits that do make a lot of sense to people like people eating probiotics to help their gut microbiome so the idea of there being beneficial bacteria on our skin that help us was not so foreign to them. But the question I got after explaining the concept was “when can we expect this solution?” and It felt bad to say that the information is not accessible yet. Even though I can not make this project a reality just yet I think it is very important to lay the groundwork and explore peoples reactions to a potential service or platform that could help you take care of your biome.





### Reflections - one foot on the ground and one in the clouds.



When you get the freedom of putting the current societal structures aside you can really let your mind run. It is so easy to get lost in speculations and fictional scenarios that offer more attractive ways of living, but it is important to remember that change rarely happens fast and to implement change you need to identify the steps within the system that would need to change for that particular future to happen.  



Ever since societies started migrating toward cities we have slowly become more disconnected to our environments. But there is something stirring in people, a longing for nature. Countless visions of the future feature humans living in green oases, in perfect harmony with nature, but our current reality still seems so far away. Even though we are seeing some fractions of those futures happening we still have a long way to go to re-establish that connection with nature. It feels like everyone is about ready for that big shift to a greener future but currently it takes time, effort and space to have that contact with nature. I want to make it easier for people to know to take care of themselves and their environment in a more self sufficient way.



I want to continue exploring the relationship between humans and their microbiome because it is something that could directly impact and potentially benefit each individual and we do need those tangible results to make the connection between us and other life forms. I think it is such an interesting subject because it is a part of us and I want to know if I could use that as a way to get people to be more mindful about their health and environment.



How do I get there? The information is there but it has not been translated yet. The complex system of bacteria have not been decoded and their effect on us hasn’t either so this project is still only speculation and the work now will be to change the image of the microorganisms in the minds of people. But with these speculative stories I can introduce people to a future where they could take care of themselves and help others heal by sharing biomes.



When talking about steps towards specific futures it sounds easy enough just to identify each one and work to make them happen, but it’s not as simple as it sounds. Each step could take you into multiple directions that lead to undefined places. Even if you are a skilled planner you will most likely not end up with the exact future that you had in mind. That uncertainty is a bit scary but also exciting because you can speculate all you want, the future will still surprise you.
