# Bits to atoms



###Our very own sassy plant

In the week of bits to atoms we got the first taste of what awaits in the fab academy during the upcoming term. The goal of this week was to make a machine with the skills we have accumulated during the last weeks. The goal of Bits to Atoms was “Make it move” everything else was an add on! It might sound like an easy task but making it move was a challenge on its own.
<br>
<br>
![](assets/Sketchbooks.jpeg)
![](assets/Plantbotv1.JPG)
![](assets/Chassis.jpeg)
![](assets/SP.jpg)

###Giving agency to nature

The idea behind the sassy plant was a continuance of the Wet My Leaf project we did in week seven. The idea of making a plant more interactive is very interesting and explores the extent of the human – plant relationship, and its possibilities. We wanted the plant to follow people around and annoy them by bumping into them and reminding people that they are not just decorative objects but living breathing beings that should be treated with respect.

So the sassy plant was born. Our attempt to giving nature a voice.
<br>
<br>
[![SP](assets/Sassyplanticon.png)](https://vimeo.com/306402791)
*the introduction video for our project*


We had some grand plans for our sassy plant bot, mainly that it would be able to move on its own, following people around, triggered by a camera. In the end a week was only enough to build prototypes and making the chassis move with a human operator but we were very pleased with the result.

Bringing these questions into the physical world feels like it has more impact than keeping the questions theoretical so the importance of developing competences in coding, prototyping and hardware is becoming more apparent to me.

My personal contributions to the project included design and construction of the different prototypes, including the final version. The body was made from cardboard which was the perfect material for our project. Easily available and light. I made the drawings of the wheels in Illustrator and laser cut them. It was interesting to make the prototypes because you plan everything out but then the most unlikely things go wrong. It was very interesting to learn by doing. I also made the logo for the project and filmed, cut and edited the video in collaboration with Jess and Kat.
<br>
<br>
![](assets/sassylogo.jpg)

###Reflections

Even though a plant on wheels feels like a silly thing it does have serious undertones. Do we feel that a moving plant is more alive than a stationary one? Can it instigate conversations about how we treat plants and our environment?

Fun and play can be an effective way to approach hard topics so I want to continue exploring that realm of possibilities.

Can we take these explorations further? How far can we go without pushing people away? What if we switched roles with the plants? How would that change the way we think and live our lives? How will we interact with plants in the future?

[Sassy plant repository](https://mdef.gitlab.io/sassy-plants/)
