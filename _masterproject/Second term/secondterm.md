---
title: Second term final
period: 9. April 2019
date: 2019-04-09 12:00:00
term: 2
published: true
---
## Human?

We are humans but the definition of being human is changing. We used to think of ourselves as solely human but now we know that humans have evolved with multiple other organisms such as bacteria, fungi and viruses. Including fundamental functions of our beings like digestion, memory and even the ability to generate energy can all be attributed to those organisms.

I want people to be informed about their microbiomes because if people possess knowledge about how to maintain their own biomes they can improve their own personal health, societal health and possibly even planetary health.


[![2term]({{site.baseurl}}/2termvideo.png)](https://vimeo.com/332967304)
video shown during presentation

## The misunderstood microbiome

The microbiome is a complex subject, so complex that we don’t really understand it yet. We do know that it is important to our health. The health of our external microbiomes has been and is under threat after decades of sterilization of our environments and over-use of anti bacterial agents. These cleaning trends have been increasing steadily due to population growth and other factors such as globalization and consumerism. We have been told that bacteria are bad countless times but thankfully that narrative is shifting. The fact is that the vast majority of the microorganisms that live with us on our bodies are harmless or beneficial and we need them to keep healthy, with a balanced biome.

With an imbalance in our external microbiomes we can develop all kinds of disorders that lessen our quality of life like auto-immune diseases, allergic reactions and pervasive skin conditions. [according to the hygiene hypothesis] I am working towards the goal of creating awareness surrounding this subject. Even though the specifics of which organism connections create that balances are not available or accessible I feel that it is very important that we start shifting the tone of the narrative and provide the public with the information that does exist.

As with so many emerging subjects and technologies it is usually academic institutions and businesses that possess the relevant information first, they are then responsible of disseminating that information to the public. It can be exhausting for the public to try to keep up with all the new and emerging trends. It can be really difficult especially when stakeholders benefit from keeping the receivers of that information in the dark. That is why I think it is important to create a narrative that challenges those who try to use misinformation to manipulate. If the public is aware of the science it will make it easier for honest and well founded research and products to move forward and harder for those who seek to manipulate.

It can be hard to get people to care about things that challenge their comfortable way of life but if I can frame the information in a way that communicates that changing can benefit each individuals health and not only that but while you take care of yourself you help society and by proxy the planet. wouldn’t you rather be able to prevent or treat an allergy or a rash in your own home rather than having to wait four months to go to the dermatologist. Saving both money and your skin from harsh chemicals.

Right now you can improve the health of your skin by taking fewer showers and washing your face without soap or other harsh chemicals. If that sounds counter-productive that is the perception I want to change. Clean (sterile) does not mean healthy. By taking those steps you improve not only your personal health but also encourage diversity of the microbiomes of the people you interact with. Furthermore by taking fewer showers and using less chemicals to clean you can save water, money and make antibiotic resistance less likely.

If we can make people more comfortable with the idea of micro-organisms it will make it easier to move future solutions for the planet forward such as food production, energy generation and personalized medicines. It is a big ask so it will be a collective effort from individuals, public and private bodies.

## Reflections

After the midterm presentations I took an 180° turn. I had been focusing on skincare. Even though it is very interesting to look into how we use skincare and the culture surrounding it, It might not be the best subject to intervene in. I want to focus on the more general narrative. To make people comfortable with the reality that we co-exist with micro-organisms and that a majority of those organisms help keep us healthy.

The fact that we need bacteria on our skin to be healthy battles the social structures and norms we have built around cleanliness (at least in western countries). The narrative is shifting and becoming more positive but it’s still foreign to most. Information about microbiomes, especially our external microbiomes is still being collected and the information about how the different species of bacteria work together is still being analyzed so for me to create a project that stays away from complete speculation is the challenge.

We know that over- and mis-use of antibiotic agents has made auto immune and allergic diseases more common and long lasting. I want to propose a change in our habits to maintain the health of our co-inhabitants and ourselves.

The critiques helped me take a step back, not changing the course of my project but to realize that I have been looking at the subject in a very serious way. I am aware that I tend to do that when I feel that a project is important and I forget to have fun with it or even explore the possibility. The final result does not have to be completely ridiculous but I want to explore that side of the subject.

The narrative is the most important aspect of my project but I still don’t have a definitive application of that narrative.

- Can it be a cultural conversation?
- Can it provoke our idea of clean?
- Can it be ridiculous/fun?
- Can I empower people to teach themselves?

<div data-configid="37951450/69444455" style="width:525px; height:197px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>

## Plan for the next 6 weeks

22 – 28 April Doing exercises with classmates to figure out what cleanliness means to them

29 April – 5 May Speak with experts to solidify narrative

6 – 12 May Physical iterations of narrative

13 – 19 May Conduct interviews

20 – 26 May  Finalizing structure and starting work on the video

27 May – 2 June Final production and wrap up of video
