---
title: Midterm 5th March
period: 5. March 2019
date: 2018-03-05 12:00:00
term: 2
published: true
---

## Presentation
It has been such a long time since I gave a lecture in front of so many people and not having practiced exactly what I was going to say made it so much harder. I guess I was nervous because I don’t have a structured idea and everything is still up in the air. It felt bad because usually I can give a good lecture but the last two months have been hazy and unfocused. I got some good feedback, both for my presentation and also for my project

When you have been working on an idea by yourself for such a long time it’s really easy to focus too much on a certain idea and lose perspective of the context. That happened to me, I don't want my project to make it easier for big corporations to make money off of people but then I focused on a company supported by one of the cosmetic giants… It wasn’t my intention and I’m glad I got the critique because now I can depart from that direction and focus more on what I wanted in the beginning.

The critiques I got for my presentation were very valid, I didn’t have enough slides to support what I was talking about in the beginning. I really need to sit down and write an elevator pitch about my project. It took too much of the 5 minutes to explain the background of my project. I think almost everyone had this problem because we have been working on it for so long and this was the first time from the final presentations in December that any visitors have seen our projects so I personally felt like I had to really explain the idea behind the project and because of that I ran out of time.

Getting blunt critiques was really good and I feel more enthusiastic moving forward even though I have to take a step back.

## Direction

![]({{site.baseurl}}/mt1.png)

Focusing on the microbiome of the skin lead me to focusing on the face. The face has it’s own biome and is something that people really care about and a majority of people have some kind of routine to care connected to their face. I want to connect the biome studies to skincare because it is something familiar and a possible entry point for me to start creating awareness about the micro organisms that we share our bodies with.

![]({{site.baseurl}}/mt2.png)

S-Biomedic is a company that is developing a line of skin care with active bacteria that supposedly balances out other bacteria that causes acne and eczema which I think would really help people that are suffering from those conditions. They are sponsored by Johnsons & Johnsons which is a gigantic company that will be happy putting a steep price on that knowledge and solutions.

![]({{site.baseurl}}/mt3.png)

The three main groups of information and research I would need to look at.

![]({{site.baseurl}}/mt4.png)

I looked at how packaging affects your perception of product. whether it’s bigger or smaller. Plastic, glass or metal. It was an interesting exercise. From the things I tried it felt better in smaller, more manageable packaging. something that felt more like medicine.

![]({{site.baseurl}}/mt5.png)

I experimented with adding some visual cues to the gel and the powder I tried just to be able to see that there was something there. It was a bit hard to imagine that it was active when it was completely clear of any visual cues.

![]({{site.baseurl}}/mt6.png)

I tried applying a sheet mask and while imagining that there were active organisms in the mask it immediately felt very uncomfortable to put on. Keeping the mask liquid from going into my eyes and my mouth was really hard so I think a full face mask is not a good way to go. I tried cutting up the mask and it immediately became more comfortable and easy to apply.

![]({{site.baseurl}}/mt7.png)

"redefining cleanliness" is a phrase that popped into my head right before the presentations and I think it is the focus of my project going forward because cleanliness has been linked to health in the past but cleanliness is something that we need to rethink and in the world of micro organisms we can discover new ways of taking care of our own health.

## Regained focus

I am completely invested in my subject and I think that connecting with our external microbiomes is really important.

Is skin care something I can continue with? How could skincare become something open and accessible for everyone? (narrow down the focus group)

Can I get people to interact with microbiomes from others?

How can I introduce the microbiome in a communal and open source way?
