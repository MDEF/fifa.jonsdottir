---
title: bio+me
period: 24. June 2019
date: 2019-06-24 12:00:00
term: 2
published: true
---

![]({{site.baseurl}}/logo-black.jpg)

The bio + me project aims to introduce and demonstrate the importance of the human microbiome, demystifying our bacteria and showing them in a more positive light. This is accomplished by designing a sampling kit for the face that empowers users to be more knowledgeable about their own health and wellbeing together with their bacteria.

With awareness and acceptance of our own ecology, we can move to a future that opens up to mutually beneficial interactions. To understand and start caring for our own microbiomes we need a strong foundation of knowledge and data. This can be achieved by testing the microbiomes consistently in a structured manner. This project aims to bring order to microbiome testing of the face to start gathering data into a database that will become a foundation for future research and development.

While there is ample information on individual species of microorganisms that reside in and on humans, there is not much known about their interactions and how they affect the function of our skin.

The kit invites you to take a closer look and discover the hidden aspects of the human body. Making the connection between what we cannot see and what we experience.

[![bio+me video]({{site.baseurl}}/video-thumbnail.png)](https://vimeo.com/344176388)
Watch the video ^

We live in a world filled with invisible micro organisms. We interact with countless bacteria, viruses and fungi during our every action. There is however limited knowledge available about which organisms we interact with and how they impact our health and our lives. I am hoping that in the future we will have a more active and intimate relationship with our personal microbiome and that relationship will help us understand how our actions and environment affect our health and well being. To be able to have an active and informed relationship with our microbiome we need to have accurate data of what micro organisms reside on and within us. Today it is a complicated, expensive and time consuming process to receive any detailed information about your microbiome but in the future it will be cheaper and more accessible to receive that kind of information.

To be able to achieve that, there needs to be a strong foundation of testing and measuring to be able to create a database. Testing oral or gut microbiomes does not require such a strict framework but the microbiome of the face is location specific. Currently there is no structured system on how to swab for samples. With this project I have tried to create a system for data collection for more accurate results to create a foundation for future applications.

![]({{site.baseurl}}/Fífa-20.jpg)

The kit is a culmination of speculative exercises and scientific inquiry. Hopefully in the future we will be more at ease when it comes to the subject of our own ecology. Aware and actively engaged in maintaining the symbiotic relationship between us.

![]({{site.baseurl}}/final kit.png)
