---
title: Design for the new
period: 26 April - 9 May 2019
date: 2019-05-9 12:00:00
term: 2
published: true
---

The subject of my master is the human microbiome. My goal is to educate and create awareness of the microorganisms that live on our skin. The biggest barrier I had to cross was to shift the negative view of bacteria to a more positive experience. During Design for the new we were given tools to dissect our ideas and exploring the future possibilities of our ideas. It is really important when imagining a future scenario to be able to imagine the steps needed to achieve your desired reality. Design for the new was presented by [Holon](http://www.holon.cat/en/)

## Practice analysis

![]({{site.baseurl}}/Current practice.png)

To change perceptions you need to be aware of the behaviors and routines that are associated with the established routines. When you have dissected and started to understand you will be better equipped to implement change.

What is a shower? How do we implement the routines that feel so mundane?

![]({{site.baseurl}}/Desired practice.png)


During the course I was able to get new perspectives on my project, the first one was when we were grouped together to map out our subjects and contexts. My groups subject was health and that was a new way for me to see my project. I had been focusing on the individual but when thinking about the subject in a larger context I found that it is also very relevant in societal health.   

![]({{site.baseurl}}/healthmap.jpg)

## Transition pathway

When creating a transition pathway to your desired scenario it is important to be able to imagine both the positive changes and the negative. Anything can happen and if you have explored many different scenarios of failure and successes it increases the likelihood of that scenario becoming reality. For my project it felt necessary to have something physical for people to touch and interact with while listening to the narrative. You should never assume that people are familiar or know what you are talking about so a physical object can aid in understanding even if it is a bit abstract.

![]({{site.baseurl}}/transition pathway.png)

During the class we chose one of the steps in my transition pathway to prototype by acting it out. We created characters and a story and finally made a storyboard. Acting out the scenario made me aware of how simple and easy it can be to imagine and materialize the reality you want. It is also a quick way to get to questions you might not have had otherwise. My experience of the acting was mixed. Having to act was absolutely draining and horrible but improvising from the questions posed was interesting. My scenario was a classroom that was learning about the microorganisms of our skin and repeating the narrative even when it’s questioned was good practice.  

![]({{site.baseurl}}/storyboard-holon.jpg)

## Reflection

The tools that were provided during the class were interesting and helpful to make the future predictions for our projects more clear and easy to structure. When you have your subject it is a great way to either narrow down the exact places you want to focus on, but at the time the classes were held we at an awkward place of our projects. We had our subject and were supposed to be narrowing the focus down to an intervention. The class opened the interest spectrum again which was a bit confusing. I am happy to have had the classes and I will most likely use the tools we were given in future projects.
