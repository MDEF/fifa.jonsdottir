---
title: Shenzhen research trip
period: 8-14 January 2019
date: 2019-01-07 12:00:00
term: 1
published: true
---
# Biology in synthetic systems

During our visit to Shenzhen we got to experience a completely different world. Before the trip I didn’t know what to expect and to be honest I didn’t think it would feel a lot different than Shanghai which I visited a year ago. I was half expecting a concrete wasteland with dronish people running from factory to factory but I was wrong. There are an imaginable amount of skyscrapers but in between all those buildings there are a lot of tall, lush trees and every so often you come across a public garden full of life and activity.  

Going there was a humbling experience. The sheer size of the city is overwhelming and kind of terrifying. Because it is comprised of many villages it has complex and distributed governance. The urban villages have transformed into clusters of skyscrapers that feel quite empty even though it is a city of 15 million people. It was also super interesting to see how the city is being developed. I personally love the 20 or 30 story residential buildings that are covered in tiles.

![]({{site.baseurl}}/skyscrapers.jpg)

The biggest challenge in my opinion was communication and navigation. Most of us were without internet and having to navigate in a language that you cannot read and don’t understand was very stressful. I would love to be able to read and speak chinese. Feeling completely voiceless in a city that large was terrifying.

## Visits

Through our visits to the different studios and factory we got to experience
the speed and precision of manufacturing made possible through hyper specialized solution studio and factory network. They are just waiting for ideas to execute and some are even ready to ship, they are just waiting for logos and a market. It feels like the people there were just about to explode with energy and enthusiasm. It was quite easy to get caught up in all the excitement.

![]({{site.baseurl}}/ledemojis.jpg)

One interesting thing for me was that they are not waiting for the next global hit but are focusing on serving smaller more specific markets, which makes sense for a country of 1,5 billion people. It’s a different way of thinking but it might produce better end results for each individual user. It is a bit scary though that they are so eager to produce and they do not think about what impact their products will have on the user, society and the environment. When we asked about sustainability the people we asked thought we meant business sustainability. We can not be too quick to judge though, because China is leading in green energy and when they decide to do something they can actually do it in a few years but in Europe it is harder to push those changes through bureaucracy and public opinion. There needs to be a good balance between systematic thinking and acting.

One of my favorite things we did during the whole trip was an exhibition that we went to at the Shenzhen Design Museum. The name of the exhibition was Craft : The Reset.  It was an exhibition featuring traditional Chinese craft, showing how people embrace the ancient methods, bringing it to a modern light. It really was an amazing exhibition and was a good contrast from the speed and modernity of the tech environment the city is so known for. My favorite piece of the exhibition was a blanket woven from damaged and discarded silk cocoons. The artist wove the blanket and placed a bead indicating the length of one cocoon which made you feel both the artist, the insect that created the thread and the material. It was a piece of fabric with a story and a message. I am so glad that we went to that exhibition and that it was there. It shows how much the Chinese value their cultural heritage.

![]({{site.baseurl}}/silkblanket.jpg)
**the silk blanket**

## Personal project

Being part of the Biology group it was quite hard to find things that were connected to my personal project but I noticed some interesting things in the trip over all. First was how Chinese people seem to regard personal health and the health of others by wearing a facial mask and limiting the risk of spreading harmful bacteria. The other was how big the skin care industry is. The only tech I found related to biology were connected to skincare (or medical). I assume that because of the pressure to look presentable has a lot to do with it but I wanted to know if it was also connected to the pollution in the air.

A lot of the skin care trends from China are making their way to the west and are becoming very popular so I am getting more and more interested in using skincare as the entry point of my project. Using skincare routines to introduce the external microbiome to people.

## Reflections

By creating connections to organisations and specialists in China we gain valuable insights on processes and decision making and hopefully we can inspire them to think of environmental sustainability as a future goal.

![]({{site.baseurl}}/chinacontrast.png)
**a random store window vs the craft exhibition**
