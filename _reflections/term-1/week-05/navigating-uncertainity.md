---
title: Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

# Navigating the Uncertainty


*I have been putting it off writing about this week as long as possible. This week has caused the most conflict in my mind since coming to Barcelona and that conflict has been hard to put into words. I still don't know if that is a good thing or a bad thing...*
<br>
<br>
The lectures we attended during the week inspired us to look deeper into the systems and to look critically at our material world. I have gained insights into conscious and unconscious interactions and it was a very good reminder that you should be aware and question the world you live in because it is designed to make you fit comfortably into a system and not to question the status quo.<br>
<br>
<br>
<br>
Monday's lecture with José Luis de Vicente was one of the most frustrating I have ever experienced. For three hours he told us about global warming and how it is already affecting our planet in horrible ways. It was information that every single person in the class had heard countless times. As he talked i felt a pit in my stomach that grew with every minute I listened, but at the same time I became angry. I was angry because I know from my studies and experience that trying to shock people into action by telling them horrible things does not work. He talked about visions for the future but his attitude felt like he had already given up on people, systems and the world. I wanted to ask him in the class if he thought speaking like this was helpful but as I realized later the power dynamic of the classroom setting made me feel like I could not question what or how he was talking and I am annoyed at myself of not speaking up.<br>
<br>
The way he talked about global warming was pessimistic and belittling. Climate change is in the back of my mind in every choice I make in my daily life and I feel guilty every time I feel like I've made a wrong choice. That feeling of guilt was amplified during the talk because I know I'm not doing enough. When you fill people with dread and tell them that every change they have made to help the earth does not make a difference you drive people away. I think we need to help people make small changes and educate them about the problems in more constructive ways. When people realize they can make a difference real change can happen. He is right about that our current society and systems are flawed but it is unrealistic to expect every single person to change the way they live and consume completely overnight, so we need to give people the opportunity to change gradually and influence others to do the same.<br>
<br>
The responsibility should also not only rest on the consumer, we need real intervention from every side. We need the public to engage and ask more of politicians and big companies and we also need strong leaders and decision makers that won't prioritize their own group of friends/benefactors or even their own country over the whole world. When I have talked about making incremental changes I have met skepticism and negativity but we need more people to engage with the problem and try to come up with solutions from different angles so if we don't start somewhere nothing will ever change.<br>
<br>
When the talk was over I appreciated that Mariana gave a small talk afterwards and reminded us that we need to look towards the future. The world is changing and while we work towards reversing the effects of global warming we also need to be prepared to tackle problems that we will face in the near future.<br>
<br>

### Reflection

For this weeks reflection I have been swinging back and forth from pessimism to optimism and I still have not landed completely. I have been reading a lot about governance and how responsibilities can be shared between government and society but it is such a complicated topic that I don't feel like I am politically inclined enough to tackle it as my main focus so I want to figure out if I can incorporate data research and storytelling into creating a better understanding of big problems for people and governing bodies.<br>
<br>

### Resources
<br>


A TED talk I recommend if you are tired of doom and gloom reporting on climate change:<br>

[How to transform apocalypse fatigue into action on global warming](https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming/transcript#t-556616)<br>
<br>


An amazing visualization of heat change in America showing that weather is not climate:<br>

[Where are America's winters warming the most?](https://www.nytimes.com/interactive/2018/03/16/climate/us-winter-warming.html)<br>
<br>


An exploration into governance:<br>

[BBC media action](https://www.bbcmediaactionilearn.com)

[River with the same rights as a human](http://time.com/4703251/new-zealand-whanganui-river-wanganui-rights/)
