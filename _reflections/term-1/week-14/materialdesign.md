---
title: Material driven design
period: 21 January - 21 February 2019
date: 2019-01-21 12:00:00
term: 1
published: true
---

## Theory vs. practice

Our world is made up of material things but almost all the things we interact with in our daily lives have been designed and created through computers. Since the 60’s the study of design has been increasingly shifting focus away from material knowledge to theory. This fact resonated with me because during my Bachelors degree in communication design we focused so much on the theory of design thinking and strategy that most times we were completely removed from the reality we were supposed to be designing for. I regret not having had more hands on experience and even though we did not directly interact with materials I feel that we could have benefited from interacting with the context of our design and the target audience. Having a "problem to solution" process can be very efficient but it can also make you ignore other possibilities and create distance from context.

We put this into practice in Valldaura where we carved a spoon from a block of wood. You learn quick that the material is the boss and you have to give in eventually. It was meditative and almost addicting to see a spoon emerge from the wood after hours of carving. By working hands on you gain knowledge that you would not get otherwise.

![]({{site.baseurl}}/spoon carving.jpg)

In summary, we are too used to design the form/scenarios and then force materials/people onto our creations. What would happen if we started from the opposite end? Start with the material and work with it until you understand it well enough to create something great.

Mette talked about Bauhaus, one of the most famous design schools in the world, most notably how the courses were structured. the first year was the most general study of materials, the second year narrowing the focus to to one of six materials and then finally starting to design with it in the third year. With that structure the students at the school were so familiar with their chosen material they could use them in completely new and innovative ways. Using raw materials is not an option anymore so we were tasked with locating and identifying material left over from industry that is overlooked and try to uncover the potential and value of that wasted material.


![]({{site.baseurl}}/bauhaus structure.png)

In our current systems there is so much waste generated from industry because of a lack of knowledge and processes to utilise that waste. We need to find new ways of using that material in a way that is not harmful for the planet.


## Mycelium of oyster mushrooms

![]({{site.baseurl}}/oystermush.png)


I knew I wanted to grow something from the beginning. First I thought about growing kombucha on some sort of fibre but we had tried to grow kombucha but all of it got infected so I thought about mycelium. I knew of some people that had been growing mycelium for other projects and mycelium is less sensitive than kombucha so I decided to go with that. When I had chosen my organism I had to find a suitable waste material for it to grow in and some of the others in the class had found a brewery nearby where they got used barley. I knew mycelium is quite happy growing in wood chips and straw so I thought that would be the perfect material to experiment with.

[Technical information about Pleurotus ostreatus](https://www.mycelia.be/en/strain-list/m-2191-pleurotus-ostreatus)

## Spent brewers grain

![]({{site.baseurl}}/brewers grain.png)

Leftover grain from beer production is also called spent grain and it is mainly made up of malted barley (husk, pericarp and seed coat) which is a rich source of lignin, cellulose, hemicellulose, lipids, and protein (Mussatto and Roberto 2006; Aliyu and Bala 2011). The main composition is around 20% proteins and 70% fibres. The rest is sugars and other minerals.

We sourced the material from a brewery a few blocks away from IAAC called Birra08 which is a relatively small brewery but they still produce 300 kg of spent grain every week so quantity of material was not the issue. The issue was drying the material fast enough to be able to work with it. We dried it two ways, in the oven for four hours and on the roof in the sun for at least 3 days.

The fresh material feels a bit sticky from the cellulose and it smells sweet and almost pungent. It varies from person to person if they like the smell or not. When the material has been allowed to sit out for a day or so it starts smelling sour so it was unavoidable to dry the material first to be able to work with it.

[Tecnical information about brewers spent grain](https://onlinelibrary.wiley.com/doi/full/10.1111/1750-3841.13794)

[More uses for spent grain](https://www.craftbeer.com/craft-beer-muses/sustainable-uses-of-spent-grain)


## Setup

Me and Rutvij were working with the mycelium so we worked together to set everything up and we wanted to grow mycelium on agar first to have a control that we used for the experiments.


Recipe for growth medium and Sterilisation protocol

Malt agar (This recipe is enough for 16 petri plates)

- Malt extract 15 g
- Agar 10 g
- Demineralised water 500 ml

pH 5,5

Sterilise all tools before starting, make sure you are working within a sterile environment.

Measure ingredients and scrape into glass bottle, pour water into bottle and swirl around until everything is mixed together. Place container into microwave and heat with 30-60 second intervals until everything is dissolved. Heat up to 85°c and then let cool. When cooled down to 55°c you can pour the plates. Work within a sterile environment while pouring the plates. Place lids on without closing the petri dish so you let the steam evaporate before closing and placing in the fridge. When the plates are set you close the lids and place upside down in fridge.

Sterilisation protocol with pressure cooker

Sterilise all your surfaces with alcohol

- Fill pressure cooker with around 2-3 cm of water from the bottom.
- Place glass wear in pot
- Turn the pot on and wait for it to boil
- When boiling turn down the heat just a bit
- When boiling again start a timer for 20 minutes
- When done turn pot off and wait for it to depressurise
- Take out glass wear and place upside down on paper towels

![]({{site.baseurl}}/setup outside.png)
_This was propper DIY bio, outside on the concrete on a heater lamp lid_

![]({{site.baseurl}}/sterile plates.jpg)
_We poured the plates outside because we were not allowed to use the burner inside in the fab lab. The wind did move the flame a bit so I don't know how sterile it was but there were almost no infections so it was good enough_

![]({{site.baseurl}}/seeded plate.jpg)
_we got the starter mycelium from the fab textiles lab who were kind enough to lend us some for our plates_

![]({{site.baseurl}}/growing plate.jpg)
_We got permission to use the aquaponics system on the ground floor as our incubator because it was the only space where we could access the plates on a daily basis with a constant temperature over 20 degrees_


note: We forgot to measure the pH value of the medium before pouring the plates but it didn't seem to have had an effect on the growth.

## Experiments

![]({{site.baseurl}}/spoiled grain.png)
_The material is very hard to store. It is wet and sugary so it is the perfect place for micro organisms to grow. It can take as little as 6 hours to spoil depending on weather conditions_

The quality I was most interested in knowing more about was how sturdy can this material be? So when looking at one variable you need to keep all the other variable constant or at least as many as possible.

For sturdiness these were the variables I tried to keep constant

- fibre size, (using different sieves)
- thickness of substrate (mm)
- moisture content (ml)
- amount of mycelium (grams) &
- temperature (°c)

![]({{site.baseurl}}/home experiment.png)
_I did this experiment at home on my stove because there were a lot of shifts in the circumstances at school and I really felt like I needed to get the experiment started_

I sifted the blended grain through three sieves with different mesh densities and the courseness is indicated with C1-C4. I measured out two weights of the material of each courseness that was indicated with 01 or 02 behind the C code. 01 = 6,5g & 02 = 13g. Water amount was 1:1. I weighed out 0,1g of mycelium for 01 and 0,2g for 02 and placed it evenly spaced on the petri dish before placing the cooled substrate into the plate. I packed the substrate lightly with a tool so the height of the substrate would be even but not tightly enough to suffocate the mycelium and closed it.

![]({{site.baseurl}}/courseness.jpg)

I made a few mistakes during this experiment. First I used tap water instead of demineralised water. second I boiled the substrate in a pot on the stove instead of using the pressure cooker.

During the middle of the experiment my landlady came home and had to use the kitchen. I packed everything up in a hurry because I didn't want to have to explain the mess I made and the mushrooms in my limited Spanish so I also did not close the samples correctly when cleaning up, I was opening and closing them outside the sterile zone.

After that I brought everything back to IAAC and waited until we had a space to work again. It was too stressful to do the experiment in a kitchen that is not mine.


## Failures

![]({{site.baseurl}}/failure.jpg)

The biggest mistake I made in my experiment was not having sterilised my the substrate well enough. I was rushing and I was not thorough enough when preparing the substrate. The thing I learned from that was that the material is very susceptible to contamination and when contaminated it the external fungi spread very fast and seemed perfectly happy with the grain as its food source. That failure lead me to the realisation that the brewery is the perfect place to prepare the substrate and grow mushrooms.

## Connection to industry

Right now spent grain is used mainly for animal feed but if not it's discarded to landfills because of how hard it is to store. I have had first hand experience how fast the material goes bad but it is an amazing material that could be used for packaging.

As beer is made in a sterile environment it isn't so far fetched that they could dedicate some space in their factories to create packaging for beer from the leftover grain. They already have protocols in place to keep things sterile and the smell of the grain does not bother them. They would need to add a press where they could press the excess liquid from the leftover grain and moulds to grow the packaging but other infrastructure is in place to make this change happen.

There is still a lot of experiments that need to be done with structural properties and moulding but if breweries could make the adjustments to create their own packaging it would save material from landfills, oil in transport and money for the breweries in external packaging cost.

[Existing mushroom packaging](https://ecovativedesign.com/packaging)

## Reflections

During this course I have realised that adapting to circumstances is a crucial skill in the design process. Like when carving the spoon the material directs the form and your movements, space and equipment does dictate your process to some extent. It depends on you to come up with inventive ways of using what you have to conduct your work. For me it was both frustrating and rewarding to put my organisation skills and imagination to the test.

Having to track down and negotiate for space and equipment has not only made me feel independent and determined but it helped me create connections with people working on similar projects. Being able to ask for advice and sharing my successes with them has been rewarding and encouraging.

The frustrations and time spent figuring out how and where we could conduct the experiments were all a part of the process. Looking back I think they were one of the biggest learning points for me. You can know a lot about a process but the reality is that the circumstances can change suddenly and you have to be willing to adapt with them.

Time is one of the most important element that has to be taken into consideration when working on projects like this. When working with living organisms you have to make peace with the fact that it will take time to get results and rushing can result in irreversible mistakes that cost you time and resources.

Working with a living organism means that you have to cater to the needs of that organism and it will communicate to you if you have succeeded in creating an environment where they can thrive. During this process I really made an emotional connection with the mycelium. When you have to take care of a living being you become invested in their growth and wellbeing and I completely fell in love with my little fluff balls.

![]({{site.baseurl}}/fluff balls.jpg)

Having previous experiences with biology I had the luxury of working in spaces dedicated to these kinds of projects and processes, equipped with tools and materials. Reflecting on the course has made me miss and appreciate the infrastructure i had access to previously. That has made me want to give others the possibility of working on projects like these by creating a community space for biology here in Barcelona. I realise that it is a massive undertaking but there are others that are very interested in that idea so Barcelona might have a community bio-space of their own in the near future.

## Continuing the experiments

After everything I have not been able to grow the mycelium successfully in the spent grain but we have gotten the permission to use a room in the atelier across the street and I have set up an incubator there to continue the experiments. Through my failures I have found out that the substrate needs to be completely sterile because it is the perfect growth medium for every other kind of mushroom too. I have the equipment and everything set up to try again.
