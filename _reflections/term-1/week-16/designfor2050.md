---
title: Design for 2050
period: 25 April - 2 May 2019
date: 2019-05-02 12:00:00
term: 2
published: true
---

If Designing for 2050 was described with five words, it would be:

![]({{site.baseurl}}/fivewords.png)

With that in mind we created our own 5 episode series in the spirit of Black mirror. The episodes included scenarios in 2050 with elements from each individual project incorporated in a holistic vision. The videos were created using GIF animations that were a quick way for us to communicate the ambiance of our narrative and add depth to our storytelling without too much effort.

![](https://media.giphy.com/media/4a7sWil1NZoRWymHJp/giphy.gif)

Creating the world of Prismatic Minds was one of the most fun I had this term. Embracing randomness by breaking up the normal subject groupings allowed us freedom of exploration. Having very limited time to create the scenario and the video we had to work fast and take quick decisions. It only helped us creating the final results because we had work fast within the group and communicate with the other groups so the 5 episode series would stay cohesive. It was a nice change from the slow and detail oriented work on our final projects.

When imagining futures, it can be easy to go to extremes. Either to the best possible scenario or towards the worst. The goal of the prismatic minds series was to imagine and create a future scenario where both were included. The future won’t be all bad but it won’t be all good either.

The 5 episode series were placed in 5 different mega cities and our choice was Tokyo. Tokyo already experiences problems connected to loneliness and an ageing population so we decided to create a scenario within those constraints.

The individual topics are linked by the general theme of health and well-being in cities taking into account both the physical and mental health of people. Building communities and human-human interaction is something that we believe will be important in the year 2050 and will be important for mental well-being.

Sensing the urban environment through AI enabled human-machine interfaces will open up knowledge in a different space which humans will be able to see to know about the environmental conditions (good and bad) and be able to enter this other space or A dual reality is therefore created through sensing ‘invisible’ data.

![]({{site.baseurl}}/kenko2.png)

Our story happens from the first person perspective of Taeko, a recently retired fashion designer that struggles with losing her sense of purpose and community. She experiences depression and declining physical health as a result and therefore starts using Kenko. Kenko is an AI interface that lets you explore the city as data. Knowing about her environment and herself is not enough so she seeks out a physical space where she is able to connect with other humans. With the help of AI and reconnection to her community she is able to start getting better.

## Good Kenko Video

[![Good Kenko]({{site.baseurl}}/good-kenko.png)](https://vimeo.com/341238982)

Working on the episodes we were influenced by the ideas of heterotopias. Heterotopias are fictional scenarios that stay grounded in reality but have a slight twist to what we know. Altering reality slightly allows the viewer to imagine themselves in said scenario without having to stretch the imagination too far. This can be an amazing tool to take your viewer on an engaging journey through your creation.
