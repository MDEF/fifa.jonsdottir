---
title: Be Your Own System
period: 19-25 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

# Living with Ideas
*With Angela Mackey and David McCullum*



### **Communicating the Unknown**

In this Masters in Design for Emergent Futures we explore and research various subjects of increasing importance to try to anticipate future needs of the individual and society. When designing for those futures we create speculative scenarios and theoretical prototypes to explain our ideas. But speculation can only take you so far. If you want to realize what effects your design will have, you can use different methods to try and bring it to life. This week we explored and tested some methods that can help you bring your ideas to reality and make you question the way you see the world.



#### Turning the Normal Upside Down

How would you function if you forgot how to interact with everything that surrounds you? We are so used to knowing how to interact with our environment that it is a quick and fun exercise to take an ordinary object and try to use it in a different way than what it was originally designed to do.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](https://media.giphy.com/media/103R2H9RN2QZgc/giphy.gif)

The first exercise of the week was to pick an object that people had brought from their homes and find a new way to use them. The seemingly uninteresting objects were laid out on a table and we chose one to work with that peaked our interest. I chose some colorful earbuds as my starting point.

![]({{site.baseurl}}/earbuds.jpg)
<br>
What attracted me to the earbuds was their shape and their material. After rolling them in my hands for a while and trying sticking them in my nose I ended up cutting them into pieces and glueing them to my arm. The material was quite nice to press and could function as some sort of tactile interface of your wearables or just an attached stress relief. It was interesting to notice how I changed my body language while I moved around the classroom. I had my arm extended outwards.I also noticed how others would interact with me while I had my arm out.

Having those colorful pieces glued to my arm felt very strange and unnatural but that is one of the goals of living with your ideas. To get past that gimmicky stage can take a while so if you want to gain realistic insights of your idea you have to stick with it until it becomes your new normal.

We got to experience some of Angelas work by playing with green screen mobile apps. It felt new and exciting to be able to project any image onto the brightly colored fabric. In one day we were completely unable to get past the "gimmicky" stage and Angela later told us that it took her months to feel normal to use this technology. So it can vary how long you need to interact with your idea for it to feel like a part of your every day life.

![]({{site.baseurl}}/greenscreen.jpg)
<br>
<br>
When we were asked if we would use that type of technology in the real world my immediate instinct was to say no. Personally I don't like wearing clothes with bright colors and the idea of being a blank canvas for anyone to to project any image onto me feels like something I won't do anytime soon.
<br>
<br>
&nbs{{site.baseurl}}p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](https://thumbs.gfycat.com/MelodicPlushGarpike-size_restricted.gif)



#### Magic?

We made some magic machines, or speculative artefacts that correlated to a human need or want.

**Machine 1:** Social Charm

**Desire:** Idealism - The need for social justice.

**Function:** This bracelet is made up of individual collectable chip "charms" that the wearer can use to interact with various social causes and get direct information about how their contribution is making an impact.

**Magic:** It might not be magic per se but if you can give people live information about how they are helping the planet and enabling them to share that with their families and neighbours is pretty magical to me.

I thought about making a wearable that was more visible but I didn't want to make social groups even more divided. So I decided to make a bracelet where people could collect the chips that were important to them. Because social justice is very subjective, what feels normal and logical to me might not be so acceptable to others. It was also important to me that it was a physical thing so people would wear their causes and interact with others that carry matching chips.
<br>
<br>
![]({{site.baseurl}}/magic1.jpg)
<br>
<br>
**Machine 2:** Microbe tales

**Desire:** Curiosity - The need to seek knowledge.

**Function:** The machine is a simple one and consists of a big swab and a module that reads the information off the swab.

**Magic:** We are so disconnected from the life that shares our bodies with us, the magic of this machine is that it gives that life a voice and when you swipe a surface, like your arm for instance and place the swab in the reading module the module will print a story that tells you what resides there.
<br>
<br>
![]({{site.baseurl}}/magic2.jpg)
<br>
<br>
#### Living with my Bacteria

![]({{site.baseurl}}/bacteriamap.jpg)
[Source](http://www.pnas.org/content/112/17/E2120)
<br>
<br>
Itterating on the second magic machine I made. I asked myself the question how can I tell others to feel positive about the microbes that live with them if I don't know how I feel about them. So I used black acrylic paint to paint bacteria maps on my arms. The black was really graphic and made me unable to forget that I have something on my arms. The acrylic paint is really good for this application because it is waterproof and pills of in pieces so it acts a bit like your skin. It wears off with use but you can clearly see the pieces where they fall.
<br>
<br>
![]({{site.baseurl}}/bacteriaarms2.jpg)<br>
<br>
![]({{site.baseurl}}/bacteriaarms1.jpg)
<br>
<br>
This experiment inspired curiosity, worry and discomfort. It was a lot of fun to see the reactions of my classmates because everyone was really curious about what the black flecks signified and most of them wanted to interact with my arms by touching them or ask questions about them (or just looked at me and waited for me to explain).
<br>
<br>
#### What did I experience?   
*How to feel an abstract concept*

I was touching my arms a lot - I liked that it had a different texture than my skin so there were visual and tactile indicators. I felt that there was something there with my fingers, not just my eyes.

I wanted to know how it felt to be constantly aware of the things that are living on my skin because even though I am aware that I have bacteria on my skin at all times it is still quite abstract because we can't see them so it takes some imagination and willpower to recognize that there are actual living organisms that call your skin home. I got super annoyed at all the flecks I was shedding and how it got everywhere

The black paint is perfect for this application because everywhere I am I leave an imprint or a circle of black specks that signify the bacteria that I am leaving behind while just doing my daily chores. It was in the gimmick phase at school but when I got home it shifts from feeling strange to feeling normal.

<br>
![]({{site.baseurl}}/shedding.png)
<br>

I took a shower Friday morning. I tried taking a normal shower without touching the paint especially and when I looked down the bottom of the shower was littered with black specks. The shower removed almost all of the flecks from my arms but it left traces of paint. It fit really well as a metaphor to how we never get rid of all the bacteria on our skin. It really helped me be aware of that there is something living with me always.

I am not sure how I feel about this experiment because it actually made the experience with the theoretical bacteria quite negative (and I might have become a little more germophobic in the process). That was the exact opposite to what I had expected in the beginning so I think I will need to try the experiment in some other way to try to have a more positive experience.

Continuing the experiment: Will I become an expert on avoiding touch and touching or will I be an expert on spreading bacteria. What if I use glitter instead of black paint? What would happen if I made other people participate with me? What will happen if I shift from me & bacteria to the world & bacteria?


### The New Normal?

When you examine your own "normal" you might find some bizarre and interesting things but you should never be scared of questioning your surroundings, you might be surprised and delighted by what you find.

Every project I did this week ended up involving something tactile and more specifically something to do with my hands. I think it helped me a lot to use these methods to bring my ideas into the physical world and actually feel some of the experiences I had made up in my head whether that was result I had imagined or not.

With these methods and experiments I would like to find a more positive and beneficial way of interacting with our own bodies and the micro organisms that share it with us.
<br>
<br>
#### Side note:

I went up to Valldaura for a visit this weekend and I guess that I have become very used to greeting people with a kiss on the cheek because during my visit I met some people from Scandinavia and when I extended my cheek for a greeting I was met with hesitation. I remembered that it is not the "normal" way of greeting strangers where I'm from and it felt kind of awkward, but at the same time it made me think of how fast we can alter our behaviours when the object, action or interaction is part of the social norm. It was a quick exchange but it sparked some questions within me about why some things are acceptable and who decides what is ok and what is not.

That uncomfortable feeling just reminded me that my normals are not necessarily other peoples normal, but also that we are capable of adapting to things that might seem strange to us now. That insight made me more positive about my bacteria project because if I can create a positive relationship with my bacteria then it might be easier to introduce that narrative to others as the "new normal".

Do you want to know a bit more about the [bacteria that lives on your skin](https://www.thoughtco.com/bacteria-that-live-on-your-skin-373528)?
