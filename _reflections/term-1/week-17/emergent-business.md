---
title: Emergent business models
period: 16 - 30 May 2019
date: 2019-05-30 12:00:00
term: 2
published: true
---


This course was tought by Javier Creus and Valeria Righi from [Ideas for Change](https://www.ideasforchange.com/). Ideas for Change is a company that focuses on helping clients develop future marketing strategies and how to maintain relevance in the unpredictable future.

During Emergent business models we were introduced to ideologies and strategies that are very useful when it comes to making your project a reality. Having a structured way of presenting and disseminating your idea is crucial if you want it to step off the page.

The most important aspects I took from this course was that you don’t have to do everything. In fact the more you are able to incorporate into existing systems and routines the likelier it is that your project will succeed.

![]({{site.baseurl}}/businessquestions.png)

One of the main tools introduced during the course was a model called pentagrowth. It is a holistic view of methods and stakeholders that one needs to keep in mind when situating a project within a system.

![]({{site.baseurl}}/pentagrowth.png)

The two assignments we did during classes were interesting. We wrote the story of our projects within a structure based on questions that leveraged the past and the future. One focusing on analysing previous innovations within our subjects, how they impacted the world and how we could learn from them. The second was a written article for a magazine in the future. The magazine format was based on questions that leveraged the past and the future and made it easier to boil down the main goal of our project.


## Reflections

I understand and agree that for change to happen you need to be aware of the market and use strategies to get your idea on the table. You need to be able to figure out who to contact and where to apply your efforts to increase the visibility of your project. The subject I was missing from the class was a discussion on ethics in business. It is great to learn ways to promote ideas you think are worth pursuing but not all ideas should have a platform, or at least people should focus on the why.

Pushing people to strive for fast growth and exponential growth is something that I wish we would stay away from. Obsession with growth has caused so many problems in the world.

As students in this master we pride ourselves in being changemakers and hopefully a force for good in the world so it’s very important that we realize that any idea can have repercussions.
