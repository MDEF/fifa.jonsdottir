---
title: Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

# Designing with Extended Intelligence

*Ramon Sangüesa & Lucas Lorenzo Peña*


To be honest, before this week I didn’t know much about AI. Most of what I knew was from news articles and other general sources but this week really sent me down the rabbit hole. Having only a week to explore this immensely complex subject feels almost laughably short and has left me with a strange mix of curiosity and unease.

When diving into the world of artificial intelligence you can expect to encounter some very difficult questions, some that force you to take a long, hard look at how we view our world and our reality. A good first step for us was to examine the word intelligence.


## What is intelligence?

We started discussing what intelligence is within groups and familiar definitions started emerging like logic, decision making, information processing and memory. We as humans have different types of intelligence. We have senses, emotional, social and cultural intelligence. But the way we think of intelligence is very anthropomorphized so we tend to think things as intelligent only if we understand them and when we put them in context with ourselves. For me the conversation became more interesting when the discussion moved away from human intelligence and we started exploring what could be considered intelligent. Like the question: Is a slime mold intelligent? Slime molds are a collection of cells that are able to take decisions based on outside stimuli without having a centralized brain but it still manages to spread in the most efficient way possible when searching for food.

![](https://media.giphy.com/media/geQ2ooe13Ew4U/giphy.gif)

The question: “What is intelligence?” reminded me of the discussion we had in the Biology Zero week about consciousness and what can be considered a living being. When we discover and learn more about a subject isn’t it reasonable that the definitions of the words we assign those subjects change with them? The term artificial intelligence does not inspire confidence or trust and in Icelandic the term directly translated is “fake intelligence” or “gervigreind” so a technology that is supposed to take care of us has a negative connotation connected to it from the get go. So augmented or extended intelligence feel like more accurate terms to describe what we have been studying this past week.


## Humans and Machines?

In recent years the relationship between humans and machines has become more ubiquitous and complex. Most people are in constant presence of their personal communication device or wearable. In those gadgets more and more AI technology is being used to monitor and collect data 24/7 and I doubt people realize how much data they are giving away and who is on the receiving end of that data. That is one of the things that make me feel the most uncomfortable in my communication with my gadgets. Companies say that they only use your data to make your experience more personalized and enjoyable but then there are leaks and it becomes clear that some companies that handle your data aren’t as honest as they said they were so me personally I want more clarity and set rules on who handles your data and for what purpose.

It is really important that we have a clear understanding of what artificial intelligence is because it is all around us. AI will become more important and more involved in people’s everyday life so giving people a basic understanding of what the technology does can make them more aware in what instances it is being used.

It is quite interesting to think about that intelligent machines have been made and developed to perform tasks like translating between languages but like in the instance of the [Google translate bots](https://www.newscientist.com/article/2114748-google-translate-ai-invents-its-own-language-to-translate-with/) that invented a more efficient code/language to communicate together it prompted the researchers to shut it down because they didn’t understand it. So my question is: will people limit the possibilities of AI learning because we want to keep it in the box of human understanding and what if we let it become smarter than us, what could we learn from the AI that we don’t already?<br>
<br>
It’s also very interesting to think that neural networks in machine learning were modeled on neurons in the brain and researchers taught the machines to process visual data like humans process visual data through their eyes but now the researchers are looking closer at other senses and modeling [new learning methods on smell](https://www.wired.com/story/artificial-intelligence-has-a-strange-new-muse-our-sense-of-smell/), which is a simpler process and has given quicker and more accurate results than the visual processes have before so the brain inspired machine learning but now the machine is inspiring scientists to look closer and learn more about the brain.

## Future - AI for good or bad?

![](https://media.giphy.com/media/duxMLhfiSctmU/giphy.gif)

Intelligent machines are great at the jobs they are programmed and trained to do and jobs like medical applications, such as pathology pattern recognition and cancer detection is a great way to utilize that technology. But pattern recognition is also used in facial recognition and social credit systems that are ongoing in China right now. Reducing humans to numbers and data just to make them more easy to control feels like a dangerous path to take for a powerful emerging technology.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“Technology is neither bad or good, it is really a reflection
of the human that is creating it”  *- Chris Duffey*

While we don’t fully understand the implications and consequences that artificial intelligence will have on people and society we should be careful about how we use it and try to make sure that the people that will be using that technology understand what lies behind.

## Environmental Implications:

One thing that Lucas mentioned at the end of the week was that all the machine learning and processing of data takes a lot of computing power and that is something that people don’t think about. In Iceland it is already becoming big business to host large data processing plants because of colder climate and relatively cheap electricity to power the computers but no one seems to have thought about the consequences of that business. China has also shown great interest in hosting servers in Iceland and I wonder if there are ethical implications to consider, I guess we will see.

## Reflections:

AI is the technology that has the most potential to revolutionize the way we live our lives but while it is still being developed and refined it feels like a murky swamp of intentions. When I was trying to write this submission my mind has been running around in circles more than normally but I guess that is because there is so much work being done in AI and it is relevant in almost every field. I feel like I have barely scratched the surface of the surface but I enjoyed this week a lot. It will be interesting to see where things go from here.

I appreciated the patience and guidance of Ramon and Lucas on the well rounded journey through the world of artificial intelligence. I feel inspired to look further and am cautiously optimistic about the future uses of AI.


### Resources

If you are an AI beginner like me, <br>
I recommend this [E-Book](https://www.awwwards.com/AI-driven-design/) that sums up a lot of the basics of AI

A really clear explanation of [Neural Networks](https://www.youtube.com/watch?v=aircAruvnKk&t=1s)

The dangers of putting an [innocent Chatbot](https://www.theverge.com/2016/3/24/11297050/tay-microsoft-chatbot-racist) online to learn from the internet

Optimistic input of the week:<br>
[Thank you for Being Late: An optimists guide to thriving in the age of accelerations.](https://www.amazon.com/Thank-You-Being-Late-Accelerations/dp/0374273537)
