---
title: Atlas of Weak Signals
period: 8 March - 11 April 2019
date: 2019-04-11 12:00:00
term: 2
published: true
---

The Atlas of weak signals course was a good and helpful addition to the masters course in my opinion. The topics provided scope and depth to my personal project but more importantly my over all interests. From the topics provided during the course I chose the topics connected to climate change and I think it was very interesting to see how connected it is to the other topics. It is such a big topic that it will touch every industry and community. Getting to explore the effects of climate change through the different topics confirmed my commitment to the subject.

Even though the topics were heavy and hard to stomach at times it is really important for us to embrace the subjects and start incorporating them into everything we do. For me that has meant exploring the maybe not so visible effects of climate change like the use of pesticides leading to the decline of insect diversity (an issue that has been known for a while but people haven’t shown much interest in).
[Insect apocalypse?](https://www.theatlantic.com/science/archive/2019/02/insect-apocalypse-really-upon-us/583018/)

## Weak Signals

![]({{site.baseurl}}/weaksignals.png)

## Personal Project

One of my favourite terms I discovered during the Atlas of weak signals course is “ecocene". Ecocene is a term that signifies a shift from the anthropocene to a society and a world that revolves around the health of the planet and the climate. It directly connects with my personal project as my subject is on the human microbiome. Empowering people to connect with their biomes and take care of the mutual health of both parties. As designers we should have a responsibility to push that narrative in every way and in every subject that we can. It is not enough to talk about it. We can encourage change by choosing to work within our own interest fields and incorporating earth friendly practices.

We need to start putting our planet first in our designs and our actions.
Designing with humility, designing with circularity, designing for the planet. The trend in design and in our language is that we always have to design better, a better product, a better service but what is better for humans is not necessarily better for the environment.

[Talk at the RCA with Daisy Ginsberg and Joanna Boehnert](https://www.youtube.com/watch?v=VE0prEndFBo&feature=youtu.be)

Creating scenarios were a good exercise, not just in the way of how to construct a narrative about a fictional scenario but how to present it in an engaging way. Explaining the steps it will take to get to that specific scenario. Whether it is gradual change or an occurrence that causes the changes. Creating scenarios that resonate with people have to be specific and have some kind of connection to our reality so it is easier to grasp.

Being introduced to data scraping was really interesting and it will be a good tool to have when doing research in the future but it was a bit difficult to use efficiently because we did not have too much time to go in depth into every topic and find articles and people at the fringe of the subjects. So when we collected the keywords from the articles of every subject they were pretty general and maybe not the weakest signals they could have been but it is still an interesting exercise to start working with data collecting and connecting the information in a more organised way. I am excited to use these tools of collecting data in the future and get even more information about my interests.

[Interactive keyword map](https://kumu.io/sraza/materials6#aowsscraped)
